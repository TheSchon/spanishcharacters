﻿var clickedEl = null;

// GA tracking
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-103322505-1']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = 'https://ssl.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

document.addEventListener("mousedown", function(event){
	//right click
    if(event.button == 2) 
	{ 
        clickedEl = event.target;
    }
}, true);

chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
    sendResponse({value: clickedEl.value});
	//clickedEl.value = clickedEl.value + request;
	var val = clickedEl.value, endIndex;
	endIndex = clickedEl.selectionEnd;
    clickedEl.value = val.slice(0, clickedEl.selectionStart) + request + val.slice(endIndex);
    clickedEl.selectionStart = clickedEl.selectionEnd = endIndex + request.length;
});